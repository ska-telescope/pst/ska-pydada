# -*- coding: utf-8 -*-
#
# This file is part of the SKA PyDADA project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module class to unpack SKA DADA files."""

from __future__ import annotations

__all__ = [
    "SkaUnpacker",
    "SKA_DIGI_SCALE_MEAN",
]

from typing import Dict, Tuple

import numpy as np
import numpy.typing as npt

from ..common import BITS_PER_BYTE, NBIT_8, NBIT_16, NBIT_FLOAT, NDIM_COMPLEX
from .unpacker_protocol import UnpackOptions

SKA_DIGI_SCALE_MEAN: Dict[int, Tuple[float, float]] = {
    1: (0.5, 0.5),
    2: (1.03, -0.5),
    4: (3.14, -0.5),
    8: (10.1, -0.5),
    16: (1106.4, -0.5),
    -32: (1.0, 0.0),
}
"""The scales and means applied to data in the SKA Generic Voltage Digitiser."""


class SkaUnpacker:
    """
    An unpacker that handles SKA packed data.

    This unpacker should be used to unpack data that comes from SKA generated files,
    such as the the flow through mode data that has been digitised.
    """

    def _rescale_and_reshape(
        self, *, data: np.ndarray, nchan: int, npol: int, ndim: int, nbit: int
    ) -> np.ndarray:
        data = data.reshape((-1, nchan, npol, ndim))

        (scale, mean) = SKA_DIGI_SCALE_MEAN[nbit]
        data = (data - mean) / scale

        if ndim == NDIM_COMPLEX:
            data = data.view(dtype=np.complex64)

        assert data.shape[-1] == 1, f"expected {data.shape=} to be 1"
        # reduce the axis
        data = np.squeeze(data, axis=-1)

        return data

    def _unpack_simple(
        self, raw_data: bytes, nchan: int, npol: int, ndim: int, nbit: int, dtype: npt.DTypeLike
    ) -> np.ndarray:
        # always cast to float, as Numpy doesn't have a non-float based complex number.
        data = np.frombuffer(raw_data, dtype=dtype).astype(np.float32)
        return self._rescale_and_reshape(data=data, nchan=nchan, npol=npol, ndim=ndim, nbit=nbit)

    def _unpack_bytes(self, raw_data: bytes, nchan: int, npol: int, ndim: int, nbit: int) -> np.ndarray:
        assert nbit in {1, 2, 4}, f"expected {nbit=} to be either 1, 2 or 4"
        bit_mask = np.int8(pow(2, nbit) - 1)
        msb = np.uint8(1 << (nbit - 1))

        values_per_byte = 8 // nbit
        values_per_sample = nchan * npol * ndim

        assert (len(raw_data) * values_per_byte) % values_per_sample == 0, (
            f"Expected number of samples={len(raw_data) * values_per_byte} to be divisible "
            f"by nchan*npol*ndim={nchan*npol*ndim}"
        )

        # the following is an optimisation to use Numpy vectorisation.
        # First duplicate value values_per_byte times and then work on
        # a slice of ranging from 0..(values_per_byte - 1) and then perform
        # the bit shifting and masking
        data = np.frombuffer(raw_data, dtype=np.uint8).astype(np.int32)
        data = np.repeat(data, values_per_byte)

        for value_shift in range(values_per_byte):
            bit_shift = nbit * value_shift
            data[value_shift::values_per_byte] >>= bit_shift
            data[value_shift::values_per_byte] &= bit_mask

        if nbit != 1:
            # This handles the twos complement of negative numbers when nbits < 8.
            # The msb is a mask for the most significant bit of NBIT. Doing a bitwise
            # AND operation we can see if the number should be negative.  If it is then cast
            # the bit_mask as an integer and flip bits and then do bitwise a OR operation on
            # the value.
            #
            # e.g NBIT = 2, msb = 0b10, bit_mask = 0b11
            # if value = 0b11, then output value should be -1, if 0b10 then value should be -2
            negative_mask = np.where(data & msb)
            data[negative_mask] |= ~int(bit_mask)

        return self._rescale_and_reshape(
            data=data.astype(np.float32), nchan=nchan, npol=npol, ndim=ndim, nbit=nbit
        )

    def unpack(self, *, data: bytes, options: UnpackOptions) -> np.ndarray:
        """Unpack SKA specific data given the unpack option.

        This returns the unpacked data as a 3 dimensional Numpy array with the following
        dimensions:

            * time
            * frequency
            * polarisation

        This may return real or complex values based on the ``NDIM`` value in the
        header. If ``NDIM`` is 1 then real floating point data is returned, if 2 then
        complex value data is returned.

        This method can only handle data in chunks being a multiple of the resolution,
        as given by ``NCHAN * NPOL * NDIM * NBIT // 8``. If the length of the data bytes
        is not a multiple of the resolution this method will ignore the remaining bytes
        of data.

        :param data: the input byte array of data.
        :type data: bytes
        :param options: the options for unpacking
        :type options: UnpackOptions
        :return: the unpacked output array of data.
        :rtype: np.ndarray
        """
        assert options is not None, "expected options not to be None"

        nbit = options.nbit
        nchan = options.nchan
        npol = options.npol
        ndim = options.ndim

        # ensure that we can read a whole byte
        num_resolutions = 1
        resolution_bits = nchan * npol * ndim * abs(nbit)
        while (num_resolutions * resolution_bits) % BITS_PER_BYTE != 0:
            num_resolutions *= 2
        resolution = (num_resolutions * resolution_bits) // BITS_PER_BYTE

        end_idx = len(data) - (len(data) % resolution)

        if "nsamp" in options.addition_args:
            nsamp = options.nsamp
            if (nsamp % num_resolutions) != 0:
                nsamp += num_resolutions - (nsamp % num_resolutions)

            nsamp_end_idx = nsamp * resolution // num_resolutions
            end_idx = min(end_idx, nsamp_end_idx)

        data = data[:end_idx]

        if nbit == NBIT_8:
            unpacked = self._unpack_simple(
                raw_data=data, nchan=nchan, npol=npol, ndim=ndim, nbit=nbit, dtype=np.int8
            )
        elif nbit == NBIT_16:
            unpacked = self._unpack_simple(
                raw_data=data, nchan=nchan, npol=npol, ndim=ndim, nbit=nbit, dtype=np.int16
            )
        elif nbit == NBIT_FLOAT:
            unpacked = self._unpack_simple(
                raw_data=data, nchan=nchan, npol=npol, ndim=ndim, nbit=nbit, dtype=np.float32
            )
        else:
            unpacked = self._unpack_bytes(raw_data=data, nchan=nchan, npol=npol, ndim=ndim, nbit=nbit)

        return unpacked
