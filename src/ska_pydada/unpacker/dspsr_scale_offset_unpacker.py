# -*- coding: utf-8 -*-
#
# This file is part of the SKA PyDADA project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module class unpack DSPSR create scale and offset files."""

from __future__ import annotations

__all__ = [
    "DspsrScalesOffsetsUnpacker",
]

import numpy as np
import pandas as pd

from ..common import NBIT_FLOAT, NDIM_2, SIZE_OF_FLOAT32, SIZE_OF_UINT64
from .unpacker_protocol import UnpackOptions

SAMPLE_OFFSET_HDR: str = "sample_offset"
"""Header to represent the offset from the first time sample when scale and offset record was calculated."""

DATA_HDR: str = "data"
"""Header constant for the name of the Numpy structured data type where the data is stored."""

CHANNEL_HDR: str = "channel"
POLARISATION_HDR: str = "polarisation"
SCALE_HDR: str = "scale"
OFFSET_HDR: str = "offset"


class DspsrScalesOffsetsUnpacker:
    """
    A class that can be used to read the DSPSR scales and offsets DADA output file.

    The DSPSR library is able to write out the scales and offsets used when rescaling
    data. The format is a block of data which has the time sample, offset from the first
    time sample in the scan, when the scales and offsets were calculated, as uint64 value,
    and then are 3 dimension array of data in channel/frequency, polarisation and finally
    the scale and offset.  The third dimension is real valued but has 2 values; this
    has the affect of NDIM=2

    This unpacker can be use to take the raw data of a DADA file and attempt to unpack
    it using the above format and options defined in a ``UnpackOptions`` data class instance.
    """

    def unpack(self, *, data: bytes, options: UnpackOptions) -> np.ndarray:
        """
        Unpack DSPSR scales and offsets.

        This method unpacks the data based on the DSPSR scale and offset format.

        It asserts the following:
            * ``NBIT=-32`` (i.e. float32 data)
            * ``NDIM=2`` to account for scale and offset values
            * ``BLOCK_HEADER_BYTES=8`` for the sample number as a uint64
            * ``BLOCK_DATA_BYTES=NCHAN * NPOL * NDIM * sizeof(float32)``
            * The length of data is a multiple of ``BLOCK_HEADER_BYTES + BLOCK_DATA_BYTES``

        This then returns the array of bytes as a Numpy structured array with the following fields:
            * sample_offset - the sample number when the scales and offsets were calculated
            * data - a 3-dimensional array with first dimension is channel, second dimension is the
              polarisation, and the 3 dimension has 2 values the scale and offset.

        :param data: the input byte array of data.
        :type data: bytes
        :param options: the options for unpacking
        :type options: UnpackOptions
        :return: the unpacked output array of data.
        :rtype: np.ndarray
        """
        assert options is not None, "expected options not to be None"

        nbit = options.nbit
        assert (
            nbit == NBIT_FLOAT
        ), f"expected the NBIT of the file to be {NBIT_FLOAT} (i.e. float32) but was {nbit}"

        nchan = options.nchan
        npol = options.npol

        ndim = options.ndim
        assert ndim == NDIM_2, (
            "expected the NDIM of the file to be 2, one value for the scale and one for offset,"
            f" but was {ndim}"
        )

        block_header_bytes = options.block_header_bytes
        assert block_header_bytes == SIZE_OF_UINT64, (
            "expected BLOCK_HEADER_BYTES of the file to be size of "
            f"a uint64 in bytes (i.e. {SIZE_OF_UINT64})"
        )

        block_data_bytes = options.block_data_bytes
        expected_block_data_bytes = nchan * npol * ndim * SIZE_OF_FLOAT32
        assert block_data_bytes == expected_block_data_bytes, (
            f"expected BLOCK_HEADER_BYTES of the file to be {expected_block_data_bytes} "
            f" but was {block_data_bytes}"
        )

        resolution = block_header_bytes + block_data_bytes

        assert len(data) % resolution == 0, f"expected {len(data)=} to be a multiple of {resolution=}"

        # create a custom Numpy data type
        dt = np.dtype([(SAMPLE_OFFSET_HDR, np.uint64), (DATA_HDR, np.float32, (nchan, npol, ndim))])
        return np.frombuffer(buffer=data, dtype=dt)

    @staticmethod
    def convert_to_dataframe(data: np.ndarray) -> pd.DataFrame:
        """
        Convert a scale and offset structured array as a Pandas dataframe.

        This method expects that the data array is Numpy structured array
        that has been return from the :py:meth:`unpack` method. A handcrafted
        array will need to ensure that the Numpy ``dtype`` is of the following:

        .. code-block:: python

            np.dtype([("sample_offset", np.uint64), ("data", np.float32, (nchan, npol, ndim))])

        The output data frame would provide a data frame like the following:

        .. code-block:: text

                sample_offset  channel  polarisation     scale    offset
            0               0        0             0  0.967820 -0.981651
            1               0        0             1  0.481840 -0.935354
            2               0        1             0  0.350116 -0.883749
            3               0        1             1  0.255031 -1.160583
            4               0        2             0  0.189749 -0.904989
            ..            ...      ...           ...       ...       ...
            59           2048       29             1  0.034914 -1.645761
            60           2048       30             0  0.035413 -0.855303
            61           2048       30             1  0.033623 -1.066285
            62           2048       31             0  0.032296 -2.397925
            63           2048       31             1  0.031079 -3.250559

        :param data: the data array to convert to a Pandas data frame.
        :type data: np.array
        :return: a data frame that has columns of ``sample_offset``, ``channel``, ``polarisation``
            ``scale`` and ``offset``.
        :rtype: pd.DataFrame
        """
        dt = data.dtype
        dt_names = dt.names
        expected_names = (SAMPLE_OFFSET_HDR, DATA_HDR)
        assert (
            dt_names == expected_names
        ), f"expected data to be a structured Numpy array with {expected_names=} but got {dt_names=}"
        assert (
            dt[SAMPLE_OFFSET_HDR] == np.uint64
        ), f"expected the sample field of data type to be a uint64 but got {dt[SAMPLE_OFFSET_HDR]=}"

        (data_dtype, data_shape) = dt[DATA_HDR].subdtype
        assert data_dtype == np.float32, f"expected the data type to be of float32 but got {data_dtype}"
        assert len(data_shape) == 3, f"expected that data to be 3 dimensional array but got {len(data_shape)}"
        (nchan, npol, ndim) = data_shape
        assert ndim == 2, f"expected last dimension of data to be 2 but got {ndim=}"

        def _row_as_dataframe(sample_offset: np.uint64, row_data: np.ndarray) -> pd.DataFrame:
            df = pd.concat([pd.DataFrame(chan_data) for chan_data in row_data], keys=np.arange(nchan))

            # Note the output file is order with offset then scale - but will be fixed in AT3-925
            df = df.reset_index(names=[CHANNEL_HDR, POLARISATION_HDR]).rename(
                columns={0: OFFSET_HDR, 1: SCALE_HDR}
            )

            # add a header with the value of the current sample offset
            df[SAMPLE_OFFSET_HDR] = sample_offset

            # Get a data frame with the headers in the correct order
            return df[[SAMPLE_OFFSET_HDR, CHANNEL_HDR, POLARISATION_HDR, SCALE_HDR, OFFSET_HDR]]

        return pd.concat([_row_as_dataframe(sample_offset, row_data) for (sample_offset, row_data) in data])
