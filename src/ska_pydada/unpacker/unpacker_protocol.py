# -*- coding: utf-8 -*-
#
# This file is part of the SKA PyDADA project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Module class for defining a Python Protocol that can be used to unpack DADA files."""

from __future__ import annotations

__all__ = [
    "Unpacker",
    "UnpackOptions",
]

import dataclasses
from typing import Any, Protocol

import numpy as np


@dataclasses.dataclass(kw_only=True)
class UnpackOptions:
    """A data class that defines the options to use during unpacking of data."""

    nbit: int
    """
    The number of bits per sample value.

    Note that this value is for 1 dimension of the data.  The total
    number of bits for a sample value is ``nbits * ndim``.
    """

    nchan: int
    """The number of channels per time sample."""

    npol: int
    """The number of polarisations per channel."""

    ndim: int
    """
    The number of dimensions for the sample value.

    If ``ndim=1`` then the data are real valued and if ``ndim=2`` then the data are complex valued.
    """

    block_header_bytes: int | None = None
    """
    The number of bytes in the header of each chunk of data.

    If not set but the ``block_data_bytes`` value is set, then this will default to 0.
    """

    block_data_bytes: int | None = None
    """
    The number of bytes in the data section of each chunk of data.

    It's invalid for ``block_header_bytes`` but this not to be set.  However, if this
    property is set but ``block_header_bytes`` is not set then that property is treated as
    having a value of zero.
    """

    addition_args: dict = dataclasses.field(default_factory=dict)
    """Additional arguments that may specific to a given unpacker."""

    def __post_init__(self: UnpackOptions) -> None:
        """Perform post initialisation validation."""
        if self.block_header_bytes is not None:
            assert (
                self.block_data_bytes is not None
            ), "expected block_data_bytes to be set when block_header_bytes is set"
        else:
            if self.block_data_bytes is not None:
                self.block_header_bytes = 0

    def __getattr__(self: UnpackOptions, key: str) -> Any:
        """Get an attribute given the key.

        :param key: the attribute key to get
        :type key: str
        :return: the attribute value, if it exists
        :rtype: Any
        :raises AttributeError: if attribute is not found
        """
        if key in self.addition_args:
            return self.addition_args[key]

        raise AttributeError(f"'UnpackOptions' object has no attribute '{key}'")


class Unpacker(Protocol):
    """A protocol that class should implement to unpack bytes of data.

    The input data is expected to be in time, frequency and polarisation (TFP) ordering.
    The output data is also in TFP.
    """

    def unpack(self, *, data: bytes, options: UnpackOptions) -> np.ndarray:
        """
        Unpack the data that is assumed to be a buffer of unsigned int8 (i.e. bytes).

        Implementations of this protocol can be used to convert from a byte array of data to the
        unpackers

        The input data is expected to be in time, frequency and polarisation (TFP) ordering.
        The output data is also in TFP.

        :param data: the input data array of bytes
        :type data: bytes
        :param options: the unpack options. This may be specific to the unpacker that may
            need additional options.
        :type options: UnpackOptions
        :return: the unpacked array of data in TFP ordering.
        :rtype: np.ndarray
        """
