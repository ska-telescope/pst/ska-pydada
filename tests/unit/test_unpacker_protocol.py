# -*- coding: utf-8 -*-
#
# This file is part of the SKA PyDADA project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.

"""This file contains unit tests for the UnpackOptions in unpacker_protocol.py."""


import pytest

from ska_pydada import UnpackOptions


def test_unpack_options_with_additional_arg() -> None:
    """Test that additional arguments can be accessed as attributes on a UnpackOptions instance."""
    additional_ags = {
        "luke": "vader",
        "arthur": 42,
    }

    options = UnpackOptions(nbit=1, nchan=2, npol=2, ndim=2, addition_args=additional_ags)

    assert options.luke == "vader"
    assert options.arthur == 42

    with pytest.raises(AttributeError, match="'UnpackOptions' object has no attribute 'bob'"):
        options.bob


def test_unpack_options_with_block_options() -> None:
    """Test UnpackOptions if the block header/data options are set or not set."""
    # test when both header and data bytes values are set there is no exception
    options = UnpackOptions(nbit=1, nchan=2, npol=2, ndim=2, block_header_bytes=8, block_data_bytes=32)

    # test header bytes is set to 0 if not set but block_data_bytes is set
    options = UnpackOptions(nbit=1, nchan=2, npol=2, ndim=2, block_data_bytes=32)
    assert options.block_header_bytes == 0

    # test that an assertion exception is raise if there is a block_header_bytes set but no data bytes
    with pytest.raises(AssertionError):
        UnpackOptions(nbit=1, nchan=2, npol=2, ndim=2, block_header_bytes=8)
