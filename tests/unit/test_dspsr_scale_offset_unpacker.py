# -*- coding: utf-8 -*-
#
# This file is part of the SKA PyDADA project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.

"""This file contains unit tests for the DSPSR Scale Offset unpacker in dspsr_scale_offset_unpacker.py."""

import numpy as np
import pytest

from ska_pydada import (
    BLOCK_DATA_BYTES,
    BLOCK_HEADER_BYTES,
    NBIT,
    NBIT_FLOAT,
    NCHAN,
    NDIM,
    NDIM_2,
    NPOL,
    RESOLUTION,
    SIZE_OF_FLOAT32,
    SIZE_OF_UINT64,
    AsciiHeader,
    DadaFile,
    DspsrScalesOffsetsUnpacker,
    UnpackOptions,
)


@pytest.fixture
def nchan() -> int:
    """Get the number of channels."""
    return 32


@pytest.fixture
def npol() -> int:
    """Get the number of polarisations."""
    return 2


@pytest.fixture
def samples_per_chunk() -> int:
    """Get the number of samples per rescale calculation."""
    return 1024


@pytest.fixture
def num_chunks() -> int:
    """Get the number of rescale calculations."""
    return 1


@pytest.fixture
def rescale_data(nchan: int, npol: int, num_chunks: int, samples_per_chunk: int) -> np.ndarray:
    """Get randomly generated rescale data."""
    dtype = np.dtype([("sample_offset", np.uint64), ("data", np.float32, (nchan, npol, NDIM_2))])

    rescale_data = [
        (np.uint64(idx * samples_per_chunk), np.random.rand(nchan, npol, NDIM_2).astype(np.float32))
        for idx in range(num_chunks)
    ]

    return np.array(rescale_data, dtype=dtype)


@pytest.fixture
def dada_file(nchan: int, npol: int, rescale_data: np.ndarray) -> DadaFile:
    """Get a randomly generated DadaFile."""
    block_data_bytes = nchan * npol * NDIM_2 * SIZE_OF_FLOAT32
    resolution = block_data_bytes + SIZE_OF_UINT64

    assert rescale_data.dtype.itemsize == resolution

    header = AsciiHeader()
    header[NDIM] = NDIM_2
    header[NCHAN] = nchan
    header[NPOL] = npol
    header[NBIT] = NBIT_FLOAT
    header[BLOCK_HEADER_BYTES] = SIZE_OF_UINT64
    header[BLOCK_DATA_BYTES] = block_data_bytes
    header[RESOLUTION] = resolution

    return DadaFile(header=header, raw_data=rescale_data.tobytes())


@pytest.fixture
def unpack_options(nchan: int, npol: int) -> UnpackOptions:
    """Get valid unpack options for given nchan and npol."""
    return UnpackOptions(
        nbit=NBIT_FLOAT,
        nchan=nchan,
        npol=npol,
        ndim=NDIM_2,
        block_header_bytes=SIZE_OF_UINT64,
        block_data_bytes=nchan * npol * NDIM_2 * SIZE_OF_FLOAT32,
    )


@pytest.mark.parametrize(
    "nchan,npol,num_chunks,samples_per_chunk",
    [
        (32, 2, 3, 1024),
        (64, 1, 2, 8196),
        (128, 2, 1, 512),
    ],
)
def test_dspsr_scale_offset_unpacker(
    nchan: int,
    npol: int,
    num_chunks: int,
    rescale_data: np.ndarray,
    dada_file: DadaFile,
) -> None:
    """Test happy path of unpacking."""
    unpacker = DspsrScalesOffsetsUnpacker()
    unpacked_data = dada_file.unpack_tfp(unpacker=unpacker)

    np.testing.assert_equal(unpacked_data, rescale_data)

    df = DspsrScalesOffsetsUnpacker.convert_to_dataframe(unpacked_data)
    expected_df_size = num_chunks * nchan * npol
    assert df.shape[0] == expected_df_size

    df = df.set_index(["sample_offset", "channel", "polarisation"], drop=True)

    for sample, row in rescale_data:
        for ichan in range(nchan):
            for ipol in range(npol):
                expected_offset = row[ichan, ipol, 0]
                expected_scale = row[ichan, ipol, 1]

                curr_df = df.loc[int(sample), ichan, ipol]  # type: ignore
                assert curr_df["scale"] == expected_scale
                assert curr_df["offset"] == expected_offset


def test_dspsr_scale_offset_unpacker_invalid_nbit(dada_file: DadaFile, unpack_options: UnpackOptions) -> None:
    """Test the unpacker throws exception when invalid nbit."""
    unpack_options.nbit = 4

    with pytest.raises(AssertionError) as exc_info:
        dada_file.unpack_tfp(unpacker=DspsrScalesOffsetsUnpacker(), options=unpack_options)

    err = exc_info.value
    assert str(err) == f"expected the NBIT of the file to be -32 (i.e. float32) but was {unpack_options.nbit}"


def test_dspsr_scale_offset_unpacker_invalid_ndim(dada_file: DadaFile, unpack_options: UnpackOptions) -> None:
    """Test the unpacker throws exception when invalid ndim."""
    unpack_options.ndim = 1

    with pytest.raises(AssertionError) as exc_info:
        dada_file.unpack_tfp(unpacker=DspsrScalesOffsetsUnpacker(), options=unpack_options)

    err = exc_info.value
    assert str(err) == (
        "expected the NDIM of the file to be 2, one value for the scale and one for offset,"
        f" but was {unpack_options.ndim}"
    )


def test_dspsr_scale_offset_unpacker_invalid_block_header_bytes(
    dada_file: DadaFile, unpack_options: UnpackOptions
) -> None:
    """Test the unpacker throws exception when invalid block_header_bytes."""
    unpack_options.block_header_bytes = 0

    with pytest.raises(AssertionError) as exc_info:
        dada_file.unpack_tfp(unpacker=DspsrScalesOffsetsUnpacker(), options=unpack_options)

    err = exc_info.value
    assert str(err) == (
        "expected BLOCK_HEADER_BYTES of the file to be size of " f"a uint64 in bytes (i.e. {SIZE_OF_UINT64})"
    )


def test_dspsr_scale_offset_unpacker_invalid_block_data_bytes(
    dada_file: DadaFile, unpack_options: UnpackOptions
) -> None:
    """Test the unpacker throws exception when invalid block_data_bytes."""
    unpack_options.block_data_bytes += 1  # type: ignore

    with pytest.raises(AssertionError) as exc_info:
        dada_file.unpack_tfp(unpacker=DspsrScalesOffsetsUnpacker(), options=unpack_options)

    err = exc_info.value
    assert str(err) == (
        f"expected BLOCK_HEADER_BYTES of the file to be {unpack_options.block_data_bytes - 1} "
        f" but was {unpack_options.block_data_bytes}"
    )


def test_dspsr_scale_offset_unpacker_incorrect_data_length(
    dada_file: DadaFile, unpack_options: UnpackOptions
) -> None:
    """Test assertion that the unpacker expects data size to be a multiple of the resolution."""
    dada_file._raw_data = dada_file._raw_data[:-3]

    resolution = unpack_options.block_data_bytes + unpack_options.block_header_bytes  # type: ignore
    with pytest.raises(AssertionError) as exc_info:
        dada_file.unpack_tfp(unpacker=DspsrScalesOffsetsUnpacker(), options=unpack_options)

    err = exc_info.value
    assert str(err) == (f"expected len(data)={len(dada_file.raw_data)} to be a multiple of {resolution=}")
