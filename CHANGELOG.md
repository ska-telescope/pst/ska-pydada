# Changelog

## (unreleased)

### New

* AT3-914 add ability to unpack a DSPSR scales and offsets DADA file

### Change

### Fix

### Other

## 0.1.0

### New

* AT3-682 initial setup of ska-pydada project
* AT3-683 add handling of reading DADA files, including header and data
* AT3-684 add notebook for demonstrating polyphase filter bank (PFB) inversion of impulses
* AT3-685 add frequency ramp validation
* AT3-754 add a data unpacker submodule

### Change

### Fix

* AT3-793 fix the digitisation offset for SKA Unpacker

### Other
